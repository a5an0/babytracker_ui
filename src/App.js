import React, { Component } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ReactLoading from 'react-loading';
import './App.css';
import '../node_modules/react-vis/dist/style.css';
import {FlexibleWidthXYPlot, XYPlot, MarkSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis} from 'react-vis';

const ENDPOINT = "https://7nsuaf8jac.execute-api.us-east-1.amazonaws.com/staging/";

function Message(props) {
    if (props.message === "") {
        return <ReactLoading type="bubbles" color="#000000" />;
    } else {
        return <p>{props.message}</p>;
    }
}

function FeedingChart(props) {
    return (
            <FlexibleWidthXYPlot height={300} xType='time' yDomain={[0,180]}>
            <VerticalGridLines />
            <HorizontalGridLines />
            <XAxis />
            <YAxis />
            <MarkSeries data={props.data}  />
            </FlexibleWidthXYPlot>
    )
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currFeeding:"",
            lastFeeding: "",
            totalFeeding: "",
            feedingData: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    notify = () => toast("Saved Feeding!", {
        position: toast.POSITION.BOTTOM_CENTER,
        type: toast.TYPE.SUCCESS
    });

    fetchLastFeeding = () => {
        fetch(ENDPOINT + "feeding")
            .then(response => response.json()
                  .then(data => this.setState({lastFeeding: data.message})));
    }

    fetchTotalFeeding = () => {
        fetch(ENDPOINT + "today")
            .then(response => response.json()
                  .then(data => this.setState({totalFeeding: data.message})));
    }

    fetchFeedingData = () => {
        fetch(ENDPOINT + "today_data")
            .then(response => response.json()
                  .then(data => this.setState({feedingData: data.feedings.map(
                      d => ({x: new Date(d[0]), y:d[1]})
                  )})))
            .then(() => console.log(this.state.feedingData));
    }

    handleChange(event) {
        this.setState({currFeeding: event.target.value});
    }

    handleSubmit(event) {
        if (this.state.currFeeding === "") {
            event.preventDefault();
            return;
        }
        fetch(ENDPOINT + "feeding",
              {method: 'POST',
               mode: 'cors',
               headers: {
                   'Content-Type': 'application/json'
               },
               body: JSON.stringify({'amount': Number(this.state.currFeeding)})
              }
             ).then(() => console.log("Done!"))
            .then(() => this.notify())
            .then(this.fetchLastFeeding)
            .then(this.fetchTotalFeeding)
            .then(this.fetchFeedingData)
            .then(() => this.setState({"currFeeding": ""}));
        event.preventDefault();
    }

    componentDidMount() {
        this.fetchLastFeeding();
        this.fetchTotalFeeding();
        this.fetchFeedingData();
        this.lastTimer =setInterval(
            () => this.fetchLastFeeding(),
            1000 * 30);
        this.totalTimer =setInterval(
            () => this.fetchTotalFeeding(),
            1000 * 30);
    }

    componentWillUnmount() {
        clearInterval(this.lastTimer);
        clearInterval(this.totalTimer);
    }

    render () {
        const data = [
            {x: 0, y: 8},
            {x: 1, y: 5},
            {x: 2, y: 4},
            {x: 3, y: 9},
            {x: 4, y: 1},
            {x: 5, y: 7},
            {x: 6, y: 6},
            {x: 7, y: 3},
            {x: 8, y: 2},
            {x: 9, y: 0}
        ];
        return (
                <div className="container">
                <Message message={this.state.lastFeeding}/>
                <Message message={this.state.totalFeeding} />
                <FeedingChart data={this.state.feedingData} />
                <form onSubmit={this.handleSubmit}>
                <div className="input-group mb-3">
                <input type="number" placeholder="Feeding Amount (in ml)" className="form-control" value={this.state.currFeeding} onChange={this.handleChange} pattern="\d*" />
                <div className="input-group-append">
                <input type="submit" value="Save" className="form-control btn btn-primary" />
                </div>
                </div>
                </form>
                <ToastContainer />
            </div>
        );
    }
}

export default App;
